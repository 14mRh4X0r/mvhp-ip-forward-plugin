package me.l4mrh4x0r.mvhp;

import com.google.inject.Inject;
import net.minecraft.server.network.ServerLoginPacketListenerImpl;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.logging.log4j.Logger;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.Server;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.lifecycle.RefreshGameEvent;
import org.spongepowered.api.event.lifecycle.StartingEngineEvent;
import org.spongepowered.api.event.network.ServerSideConnectionEvent;
import org.spongepowered.common.accessor.network.ConnectionAccessor;
import org.spongepowered.common.bridge.network.ConnectionBridge;
import org.spongepowered.configurate.CommentedConfigurationNode;
import org.spongepowered.configurate.ConfigurateException;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.ConfigurationOptions;
import org.spongepowered.configurate.loader.ConfigurationLoader;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;
import org.spongepowered.configurate.objectmapping.meta.Comment;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;
import org.spongepowered.plugin.jvm.Plugin;

import java.lang.reflect.Type;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Set;

@Plugin("mvhp-ip-forward-plugin")
public class IPForwardPlugin {

    @Inject private Logger logger;

    @Inject @DefaultConfig(sharedRoot = true)
    private Path configFile;
    @Inject @DefaultConfig(sharedRoot = true)
    private ConfigurationLoader<CommentedConfigurationNode> configLoader;
    private ConfigurationOptions configOptions;

    private Config config = Config.DEFAULT;

    private void loadConfig() {
        try {
            config = configLoader.load(configOptions).get(Config.class, Config.DEFAULT);
            logger.info("Loaded config. Trusted sources: {}", config.getTrustedSources());
        } catch (ConfigurateException e) {
            logger.warn("Could not load config, using previous or default", e);
        }
    }

    @Listener
    public void onServerStart(StartingEngineEvent<Server> event) {
        configOptions = configLoader.defaultOptions().serializers(builder ->
                builder.register(InetAddress.class, InetAddressSerializer.INSTANCE));

        try {
            if (!configFile.toFile().exists()) {
                configLoader.save(configLoader.createNode(configOptions).set(Config.DEFAULT));
            }
        } catch (ConfigurateException e) {
            logger.warn("Could not write default config", e);
        }

        loadConfig();
    }

    @Listener
    public void onReload(RefreshGameEvent event) {
        loadConfig();
    }

    @Listener
    public void onClientAuth(ServerSideConnectionEvent.Auth event) {
        var clientAddr = event.connection().address().getAddress();
        var hostName = event.connection().virtualHost().getHostString();
        if (!hostName.startsWith("\u0001")) {
            return;
        }
        if (!config.getTrustedSources().contains(clientAddr)) {
            logger.warn("[{}] Non-trusted client tried to spoof IP address: \"{}\"",
                    clientAddr, StringEscapeUtils.escapeJson(hostName));
            return;
        }

        logger.debug("[{}] Spoofing IP address: {}", clientAddr, StringEscapeUtils.escapeJson(hostName));

        var conn = ((ServerLoginPacketListenerImpl) event.connection()).connection;
        var split = hostName.substring(1).split("\u0002", 2);
        var addr = split[0].split("\u001f");
        ((ConnectionBridge) conn).bridge$setVirtualHost(split[1], event.connection().virtualHost().getPort());
        ((ConnectionAccessor) conn).accessor$address(new InetSocketAddress(addr[0], Integer.parseInt(addr[1])));
    }

    @ConfigSerializable
    public static class Config {
        public static final Config DEFAULT = new Config();

        @Comment("IP addresses from which to allow client address spoofing")
        private Set<InetAddress> trustedSources = Collections.emptySet();

        public Set<InetAddress> getTrustedSources() {
            return Collections.unmodifiableSet(trustedSources);
        }
    }

    public static class InetAddressSerializer implements TypeSerializer<InetAddress> {
        public static final InetAddressSerializer INSTANCE = new InetAddressSerializer();

        private InetAddressSerializer() {}

        @Override
        public InetAddress deserialize(Type type, ConfigurationNode node) throws SerializationException {
            try {
                return InetAddress.getByName(node.getString());
            } catch (UnknownHostException e) {
                throw new SerializationException(e);
            }
        }

        @Override
        public void serialize(Type type, @Nullable InetAddress obj, ConfigurationNode node) throws SerializationException {
            if (obj == null) return;
            node.set(obj.getHostAddress());
        }
    }
}
